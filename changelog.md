# 1.3.0
  * Add support for IPv6 / `ip6tables`

# 1.2.1
  * 🚀 public release
