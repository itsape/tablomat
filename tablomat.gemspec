# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'tablomat/version'

Gem::Specification.new do |gem|
  gem.name          = 'tablomat'
  gem.version       = Tablomat::VERSION
  gem.authors       = ['Matthias Wübbeling', 'Arnold Sykosch']
  gem.email         = ['matthias.wuebbeling@cs.uni-bonn.de', 'sykosch@cs.uni-bonn.de']
  gem.description   = 'A simple wrapper for iptables'
  gem.summary       = 'This wrapper provides an interface to iptables.'
  gem.homepage      = 'https://gitlab.com/itsape/tablomat'
  gem.license       = 'MIT'
  gem.files         = Dir['{lib,spec}/**/*']
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ['lib']

  gem.add_development_dependency 'rake', '~> 13.0.1'
  gem.add_development_dependency 'rspec', '~> 3.9.0'
  gem.add_development_dependency 'rubocop', '~> 0.88.0'
  gem.add_development_dependency 'rubocop-performance', '~> 1.7.1'
  gem.add_development_dependency 'simplecov', '~> 0.18.5'
  gem.add_development_dependency 'simplecov-cobertura', '~> 1.4', '>=1.4.1'
end
