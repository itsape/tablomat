# TABLOMAT

*A wrapper for the iptables command in ruby*

## INSTALLATION
Install from [RubyGems](https://rubygems.org/gems/tablomat):
```sh
gem install tablomat
```
