# frozen_string_literal: true

require 'tablomat/iptables/chain'

module Tablomat
  class IPTablesBase
    # Puts the Table in IPTables
    class Table
      attr_accessor :owned
      attr_reader :name, :system, :active, :chains

      def initialize(system, name, owned = true)
        @system = system
        @name = name
        @chains = {}
        # whether this table is owned/created by us or external
        @owned = owned
        @active = false
        activate if @system.active
      end

      def get_key(name)
        name.to_s.downcase[0, 28]
      end

      def chain(name, owned = true, &block)
        key = get_key name
        (@chains[key] || Chain.new(self, name, owned)).tap do |chain|
          @chains[key] = chain
          block&.call(chain)
        end
      end

      def chain_exists(name)
        key = get_key name
        return true if @chains.key? key

        false
      end

      def insert(chain_name, data, pos)
        chain chain_name do |chain|
          chain.insert(data, pos)
        end
      end

      def append(chain_name, data)
        chain(chain_name).append(data)
      end

      def delete(chain_name, data)
        chain chain_name do |chain|
          chain.delete(data)
        end
      end

      def activate(override = false)
        return unless @owned || override

        @active = true
        return if override

        @chains.each do |_name, chain|
          chain.activate
        end
      end

      def deactivate(override = false)
        return unless @owned || override

        @active = false
        return if override

        @chains.each do |_name, chain|
          chain.deactivate
        end
      end

      def print
        require 'pp'
        pp self
      end
    end
  end
end
