# frozen_string_literal: true

module Tablomat
  class IPTablesBase
    # IPTables are made of Rules
    class Rule
      attr_accessor :owned, :active, :method, :position
      attr_reader :chain, :description

      def initialize(chain, description, owned = true)
        @system = chain.table.system
        @chain = chain
        @description = description
        @items = {}
        @owned = owned
        @active = false
        @method = 'APPEND'
        @position = 0
        activate if @chain.active
      end

      def activate(override = false)
        return unless @owned || override
        return if @active

        @active = true
        return if override

        @chain.activate unless @chain.active
        apply_create
      end

      def deactivate(override = false)
        return unless @owned || override
        return unless @active

        self.active = false
        return if override

        apply_delete
      end

      def apply_create
        return unless @owned

        method = if @method == 'APPEND'
                   "-A #{@chain.name}"
                 else
                   "-I #{@chain.name} #{@position}"
                 end
        command = "#{@system.iptables_bin} -t #{@chain.table.name} #{method} #{@description}"
        @system.exec command
      end

      def apply_delete
        return unless @owned

        command = "#{@system.iptables_bin} -t #{@chain.table.name} -D #{@chain.name} #{@description}"
        @system.exec command
      end
    end
  end
end
