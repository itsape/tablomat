# frozen_string_literal: true

require 'tablomat/iptables/rule'

module Tablomat
  class IPTablesBase
    # The IPTables class is the interface to the iptables command
    class Chain
      attr_accessor :owned
      attr_reader :table, :name, :active, :rules

      def initialize(table, name, owned = true)
        @system = table.system
        @table = table
        @name = name
        @policy = 'ACCEPT'
        @rules = {}
        @rules_sorted = []
        @owned = owned
        @active = false
        activate if @table.active
      end

      def policy(action)
        # set policy as the last rule of the chain
        raise 'Unable to assign policy to non builtin chains, TODO: implement handling' unless builtin?

        @policy = action
        return unless @active

        command = "#{@table.system.iptables_bin} -t #{@table.name} -P #{@name} #{@policy}"
        @system.exec command
      end

      def rule(name, owned = true, &block)
        if name.is_a? Hash
          name = sethandling(name) if name.key?(:set)
          name = name.map { |k, v| "--#{k} #{v}" }.join(' ')
        end
        key = name.to_s.downcase
        (@rules[key] || Rule.new(self, name, owned)).tap do |rule|
          @rules[key] = rule
          block&.call(rule)
        end
      end

      def sethandling(name)
        trash = {}
        name.each do |k, v|
          trash[k] = v
          trash[:match] = trash.delete :set if trash.key?(:set)
        end
        trash
      end

      def insert(data, pos)
        rule(data) do |rule|
          rule.method = 'INSERT'
          rule.position = pos
          @rules_sorted.insert(pos - 1, rule)
          update_rules_position
          rule.activate if @active
        end
      end

      def append(data)
        rule(data) do |rule|
          @rules_sorted << rule
          rule.activate if @active
        end
      end

      def update_rules_position
        @rules_sorted = @rules_sorted.compact
        @rules_sorted.select(&:active).each_with_index do |rule, index|
          rule.position = index + 1 if (rule.position != 0) && (rule.position != (index + 1))
        end
      end

      def delete(data)
        rule = if data.is_a? Rule
                 data
               else
                 self.rule(data)
               end
        rule.deactivate if rule.active

        @rules_sorted.delete(rule)
        @rules.delete_if { |_k, v| v.description == rule.description }
      end

      def activate(override = false)
        return unless @owned || override
        return if @active

        @active = true
        return if override

        apply_create
        activate_all_rules
      end

      def deactivate(override = false)
        return unless @owned || override
        return unless @active

        @active = false
        return if override

        deactivate_all_rules
        @active = false
      end

      def apply_create
        unless exists?
          begin
            command = "#{@system.iptables_bin} -t #{@table.name} -N #{@name}"
            @system.exec command
          rescue StandardError
            puts "Error: #{$ERROR_INFO}"
          end
        end
        # apply policy if builtin chain
        return unless builtin?

        command = "#{@system.iptables_bin} -t #{@table.name} -P #{@name} #{@policy}"
        @system.exec command
      end

      def apply_delete
        return unless exists? && !builtin?

        begin
          command = "#{@system.iptables_bin} -t #{@table.name} -F #{@name}"
          @system.exec command
          command = "#{@system.iptables_bin} -t #{@table.name} -X #{@name}"
          @system.exec command
        rescue StandardError
          puts "Error removing chain #{command}, message: #{$ERROR_INFO}"
        end
      end

      def exists?
        command = "#{@system.iptables_bin} -t #{@table.name} -nL #{@name}"
        @system.exec command
        true
      rescue StandardError
        false
      end

      def builtin?
        @table.system.builtin_chains.key?(@table.name.to_sym) && @table.system.builtin_chains[@table.name.to_sym].include?(@name)
      end

      private

      def activate_all_rules
        @rules_sorted.each do |rule|
          rule.activate if !rule.nil? && !rule.active
        end
      end

      def deactivate_all_rules
        @rules_sorted.each do |rule|
          rule.deactivate if !rule.nil? && rule.active
        end
      end
    end
  end
end
