# frozen_string_literal: true

require 'tablomat/ipset/set'
require 'tablomat/exec'

module Tablomat
  # Errorclass for IPSet-Errors
  class IPSetError < StandardError; end

  # The IPSet interface
  class IPSet
    extend Exec
    attr_accessor :ipset_bin

    def initialize
      @ipset_bin = 'ipset'
      @ipset_bin = "sudo #{@ipset_bin}" if Etc.getlogin != 'root'
      @sets = {}
    end

    def matchset(flags:, option: '', negate: false, negate_option: false, set_name:)
      set_set = "--match-set #{set_name} #{flags}"
      set_set = "! #{set_set}" if negate
      option = "! #{option}" if negate_option
      "set #{set_set} #{option}"
    end

    def set(name, &block)
      name = name.to_s.downcase
      (@sets[name] || Set.new(self, name)).tap do |set|
        @sets[name] = set
        block&.call(set)
      end
      @sets[name]
    end

    def exec(cmd)
      Exec.exec(cmd)
    rescue StandardError => e
      raise IPSetError.new, e.message
    end

    def destroy_all(really_all = false)
      # destroys all sets created in this Instance unless really_all = true
      if really_all
        command = "#{@ipset_bin} destroy"
        exec(command)
      else
        @sets.each do |_name, set|
          set.destroy if set.active
        end
      end
    end

    def add(set_name:, entry_data:, add_options: '', exist: false)
      set(set_name).add(entry_data: entry_data, add_options: add_options, exist: exist)
    end

    def create(set_name:, type:, create_options: '', rangefrom: '', rangeto: '')
      set(set_name).create(type: type, create_options: create_options, rangefrom: rangefrom, rangeto: rangeto)
    end

    def del(set_name:, entry_data:, exist: false)
      set(set_name).del(entry_data: entry_data, exist: exist)
    end

    def destroy(set_name:)
      set(set_name).destroy
    end
  end
end
