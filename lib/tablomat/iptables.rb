# frozen_string_literal: true

require 'English'
require 'tablomat/iptables/table'
require 'digest'
require 'tempfile'
require 'tablomat/exec'

module Tablomat
  # The base class for the IPTables interface
  class IPTablesBase
    extend Exec
    attr_accessor :iptables_bin
    attr_reader :active, :builtin_chains, :tmp_chain

    def initialize
      # get random value for rule creation hack, iptables limits name to 28 characters
      @tmp_chain_prefix = 'tablomat'

      @builtin_chains = { filter: %w[INPUT FORWARD OUTPUT], nat: %w[PREROUTING INPUT OUTPUT POSTROUTING], mangle: %w[PREROUTING INPUT FORWARD OUTPUT POSTROUTING] }
      @tables = {}

      @active = true
    end

    def exec(cmd)
      Exec.exec(cmd)
    end

    def synchronize
      # called regularly by normalize
      command = "#{@iptables_bin}-save"
      stdout = `#{command} 2>&1`.strip << "\n"
      if $CHILD_STATUS != 0
        # throw error
        puts "Invalid return value when calling #{command}"
      end
      parse_output stdout
    rescue StandardError => e
      puts "[error] #{e}"
    end

    # rubocop:disable Metrics/AbcSize
    def parse_output(stdout)
      stdout = stdout.split("\n").reject { |s| s[0] == '#' }.join("\n")
      data = {}
      stdout.split(/^\*/).reject { |s| s == '' }.each do |ruleset|
        table, rule = ruleset.match(/(.+?)\n(.*)/m).to_a[1..-1]
        data[table] = {}

        raise "Empty ruleset in table #{table}" if table.nil?

        rule.scan(/^:(.+?)\s+/).each do |match|
          data[table][match[0]] = []
        end

        rule.scan(/^-A (.+?) (.+?)\n/).each do |match|
          data[table][match[0]] << match[1]
        end
      end
      parse_data(data)
    end
    # rubocop:enable Metrics/AbcSize

    def parse_data(data)
      data.each do |table, chains|
        t = self.table(table, false)
        t.activate true
        parse_table(t, chains)
      end
    end

    def parse_table(table, chains)
      chains.each do |chain, rules|
        c = table.chain(chain, false)
        c.activate true
        parse_chain(c, rules)
      end
    end

    def parse_chain(chain, rules)
      rules.each do |rule|
        r = chain.rule(rule, false)
        r.activate true
      end
    end

    def table(name, owned = true, &block)
      name = name.to_s.downcase
      (@tables[name] || Table.new(self, name, owned)).tap do |table|
        @tables[name] = table
        block&.call(table)
      end
    end

    # converts any given rule to the iptables internal description format by using a temporary table
    # rubocop:disable Metrics/AbcSize
    def normalize(data, table = 'filter')
      synchronize
      tmp_chain = "#{@tmp_chain_prefix}#{Digest::SHA256.hexdigest Random.rand(1024).to_s}"[0, 28]
      self.table(table).chain(tmp_chain).activate
      self.table(table).append(tmp_chain, data)
      synchronize
      normalized = data
      self.table(table).chain(tmp_chain).rules.select { |_k, r| r.owned == false }.each do |_k, r|
        normalized = r.description
      end
      self.table(table).delete(tmp_chain, data)
      self.table(table).chain(tmp_chain).deactivate
      self.table(table).chain(tmp_chain).apply_delete
      normalized
    end
    # rubocop:enable Metrics/AbcSize

    # rubocop:disable Metrics/AbcSize
    def exists(table_name, chain_name, data = nil)
      if data.nil?
        table(table_name).chain_exists(chain_name) && table(table_name).chain(chain_name).active
      else
        data = normalize data, table_name
        table(table_name).chain(chain_name).rules.count { |_k, v| normalize(v.description, table_name) == data && v.active } >= 1
      end
    end
    # rubocop:enable Metrics/AbcSize

    def insert(table_name, chain_name, data, pos)
      data = normalize data, table_name
      table(table_name).insert(chain_name, data, pos)
    end

    def append(table_name, chain_name, data)
      data = normalize data, table_name
      table(table_name).append(chain_name, data)
    end

    def delete(table_name, chain_name, data)
      data = normalize data, table_name
      table(table_name).delete(chain_name, data)
    end

    def activate
      @active = true
      @tables.each do |_name, table|
        table.activate
      end
    end

    def deactivate
      @active = false
      tables.each do |_name, table|
        table.deactivate
      end
    end

    # rubocop:disable Metrics/AbcSize
    def get_active_rules(table = 'nat', chain = 'PREROUTING')
      rrgx = /(?<key>--?[[:alpha:]-]+) (?<value>[[:alnum:]:\.]+)/.freeze
      switch_map = { '-s' => :source, '--source' => :source,
                     '-d' => :destination, '--destination' => :destination,
                     '--dport' => :dport, '--to-destination' => :to_dest,
                     '-p' => :protocol, '--protocol' => :protocol,
                     '-j' => :jump, '--jump' => :jump,
                     '--match-set' => :match }

      table(table).chain(chain).rules.filter { |_, r| r.active }.map do |_, rule|
        rule.description.to_enum(:scan, rrgx)
            .map { Regexp.last_match }
            .map { |m| [switch_map[m[:key]], m[:value]] }
            .filter { |m| m[0] }.to_h
      end
    end

    # used to easily switch rules from one src ip to another
    def switch_sources(old_src, new_src)
      @tables.to_a.each do |_kt, tbl|
        tbl.chains.to_a.each do |_kc, chn|
          next if chn.name.include? 'tablomat'

          chn.rules.to_a.each do |_key, rule|
            next unless rule.description.include? "-s #{old_src}"

            new_data = normalize(rule.description, tbl.name).sub "-s #{old_src}", "-s #{new_src}"
            pos = rule.position + 1
            delete(tbl.name, chn.name, rule.description)
            insert(tbl.name, chn.name, new_data, pos)
          end
        end
      end
    end
    # rubocop:enable Metrics/AbcSize

    def print
      require 'pp'
      pp self
    end
  end

  # The IPTables interface
  class IPTables < IPTablesBase
    def initialize
      super()
      # iptables must be in PATH
      @iptables_bin = 'iptables'
      @iptables_bin = "sudo #{@iptables_bin}" if Etc.getlogin != 'root'
      synchronize
    end
  end

  # The IP6Tables interface
  class IP6Tables < IPTablesBase
    def initialize
      super()
      # ip6tables must be in PATH
      @iptables_bin = 'ip6tables'
      @iptables_bin = "sudo #{@iptables_bin}" if Etc.getlogin != 'root'
      synchronize
    end
  end
end
