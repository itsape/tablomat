# frozen_string_literal: true

module Tablomat
  class IPSet
    # interface to manage the entrys inside the sets
    class Entry
      def initialize(set, description)
        @system = set.system
        @set = set
        @description = description
      end

      def exists?
        command = "#{@system.ipset_bin} del #{@set.name} #{@description}"
        @system.exec command
        command = "#{@system.ipset_bin} add #{@set.name} #{@description}"
        @system.exec command
        true
      rescue IPSetError => e
        raise unless e.message.include?("Element cannot be deleted from the set: it's not added")

        false
      end

      def add(add_options, exist = false)
        command = "#{@system.ipset_bin} add #{@set.name} #{@description} #{add_options}"
        command = "#{command} -!" if exist
        @system.exec command
      end

      def del(exist = false)
        command = "#{@system.ipset_bin} del #{@set.name} #{@description}"
        command = "#{command} -!" if exist
        @system.exec command
      end
    end
  end
end
