# frozen_string_literal: true

require 'tablomat/ipset/entry'

module Tablomat
  class IPSet
    # Interface to manage ipsets
    class Set
      attr_reader :name, :system, :active

      def initialize(system, name)
        @system = system
        @name = name
        @entrys = {}
        @active = false
      end

      def exists?
        command = "#{@system.ipset_bin} list #{@name}"
        @system.exec command
        true
      rescue IPSetError => e
        raise unless e.message.include?('The set with the given name does not exist')

        false
      end

      def type
        command = "#{system.ipset_bin} list #{@name}"
        stdout = `#{command} 2>&1`.strip << "\n"
        if $CHILD_STATUS != 0
          # throw error
          puts "Invalid return value when calling #{command}"
        end
        stdout = stdout.split("\n").select { |s| s.include?('Type:') }[0]
        stdout.split(/Type: /)[1]
      rescue StandardError => e
        puts "[error] #{e}"
      end

      def entry(data, &block)
        data = data.to_s.downcase
        (@entrys[data] || Entry.new(self, data)).tap do |entry|
          @entrys[data] = entry
          block&.call(entry)
        end
      end

      def add(entry_data:, add_options: '', exist: false)
        entry(entry_data).add(add_options, exist)
      end

      def del(entry_data:, exist: false)
        entry(entry_data).del(exist)
      end

      def create(type:, create_options: '', rangefrom: '', rangeto: '')
        create_options = "range #{rangefrom}-#{rangeto} #{create_options}" if type.include?('bitmap')
        command = "#{@system.ipset_bin} create #{@name} #{type} #{create_options}"
        @system.exec command
        @active = true
      end

      def destroy
        command = "#{@system.ipset_bin} destroy #{@name}"
        @system.exec command
        @active = false
      end
    end
  end
end
