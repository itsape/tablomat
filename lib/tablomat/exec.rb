# frozen_string_literal: true

require 'tempfile'
require 'English'

# Module defined in exec.rb File to execute Commands
module Exec
  def self.exec(cmd)
    errfile = Tempfile.new('tablomat')
    stdout = `#{cmd} 2> #{errfile.path}`
    unless $CHILD_STATUS.success?
      err = errfile.read
      errfile.close
      errfile.unlink
      raise err.to_s
    end
    stdout
  end
end
