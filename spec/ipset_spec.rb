# frozen_string_literal: true

require 'spec_helper'
require 'etc'

describe Tablomat::IPSet do
  describe 'Initialize module' do
    @ipset = Tablomat::IPSet.new
  end

  before(:all) do
    @command = 'ipset'
    @command = "sudo #{@command}" if Etc.getlogin != 'root'
    @ipset = Tablomat::IPSet.new
  end

  before(:each) do
    @ipset.destroy_all(true)
  end

  after(:all) do
    # destroy all sets
    @ipset.destroy_all(true)
  end

  it 'creates new sets of type hash:ip' do
    @ipset.create(set_name: 'test1', type: 'hash:ip')
    expect(@ipset.set('test1').exists?).to be_truthy
  end

  it 'can destroy existing sets' do
    @ipset.create(set_name: 'test2', type: 'hash:ip')
    expect(@ipset.set('test2').exists?).to be_truthy
    @ipset.destroy(set_name: 'test2')
    expect(@ipset.set('test2').exists?).to be_falsey
  end

  it 'set() returns instance of Set' do
    expect(@ipset.set('test2').exists?).to be_falsey
    set_set = @ipset.set('test2')
    set_set.create(type: 'hash:ip')
    expect(@ipset.set('test2').exists?).to be_truthy
  end

  it 'checks if set exists' do
    # check precondition
    expect(@ipset.set('test3').exists?).to be_falsy

    # check existing
    @ipset.create(set_name: 'test3', type: 'hash:ip')
    expect(@ipset.set('test3').exists?).to be_truthy

    # check missing
    @ipset.destroy(set_name: 'test3')
    expect(@ipset.set('test3').exists?).to be_falsy
  end

  it 'can not create set if set with same name already exists' do
    @ipset.create(set_name: 'test4', type: 'hash:ip')
    expect(@ipset.set('test4').exists?).to be_truthy
    expect { @ipset.create(set_name: 'test4', type: 'hash:port') }.to raise_error(Tablomat::IPSetError)
  end

  it 'can not delete set if the set does not exist' do
    expect(@ipset.set('test6').exists?).to be_falsey
    expect { @ipset.destroy(set_name: 'test6') }.to raise_error(Tablomat::IPSetError)
  end

  it 'appends entrys to set in ipset' do
    @ipset.create(set_name: 'test6', type: 'hash:ip')
    expect(@ipset.set('test6').entry('192.0.2.42').exists?).to be_falsy
    @ipset.add(set_name: 'test6', entry_data: '192.0.2.42')
    expect(@ipset.set('test6').entry('192.0.2.42').exists?).to be_truthy
  end

  it 'appends entrys to set in set' do
    @ipset.create(set_name: 'test6', type: 'hash:ip')
    expect(@ipset.set('test6').entry('192.0.2.52').exists?).to be_falsy
    @ipset.set('test6').add(entry_data: '192.0.2.52')
    expect(@ipset.set('test6').entry('192.0.2.52').exists?).to be_truthy
  end

  it 'can delete entrys in sets in ipset' do
    @ipset.create(set_name: 'test7', type: 'hash:ip')
    @ipset.add(set_name: 'test7', entry_data: '192.0.2.42')
    expect(@ipset.set('test7').entry('192.0.2.42').exists?).to be_truthy
    @ipset.del(set_name: 'test7', entry_data: '192.0.2.42')
    expect(@ipset.set('test7').entry('192.0.2.42').exists?).to be_falsy
  end

  it 'can delete entrys in sets in set' do
    @ipset.create(set_name: 'test7', type: 'hash:ip')
    @ipset.set('test7').add(entry_data: '192.0.2.42')
    expect(@ipset.set('test7').entry('192.0.2.42').exists?).to be_truthy
    @ipset.set('test7').del(entry_data: '192.0.2.42')
    expect(@ipset.set('test7').entry('192.0.2.42').exists?).to be_falsy
  end

  it 'creates set with type bitmap:port' do
    @ipset.create(set_name: 'test8', type: 'bitmap:port', rangefrom: 500_00, rangeto: 600_00)
    expect(@ipset.set('test8').exists?).to be_truthy
    expect(@ipset.set('test8').type == 'bitmap:port').to be_truthy
  end

  it 'creates set with type hash:ip,port' do
    @ipset.create(set_name: 'test9', type: 'hash:ip,port')
    expect(@ipset.set('test9').exists?).to be_truthy
    expect(@ipset.set('test9').type == 'hash:ip,port').to be_truthy
  end

  it 'can destroy created in one instance' do
    # creating sets in an instance
    @ipset.create(set_name: 'test10', type: 'bitmap:port', rangefrom: 500_00, rangeto: 600_00)
    expect(@ipset.set('test10').exists?).to be_truthy
    @ipset.create(set_name: 'test11', type: 'hash:mac')
    expect(@ipset.set('test11').exists?).to be_truthy
    # creating sets in another instance
    ipsetnext = Tablomat::IPSet.new
    ipsetnext.create(set_name: 'test12', type: 'bitmap:port', rangefrom: 500_00, rangeto: 600_00)
    expect(@ipset.set('test12').exists?).to be_truthy
    ipsetnext.create(set_name: 'test13', type: 'hash:ip,port')
    expect(@ipset.set('test13').exists?).to be_truthy
    ipsetnext.create(set_name: 'test14', type: 'hash:net')
    expect(@ipset.set('test14').exists?).to be_truthy
    # destroy sets created with ipsetnext
    ipsetnext.destroy_all
    expect(@ipset.set('test10').exists?).to be_truthy
    expect(@ipset.set('test11').exists?).to be_truthy
    expect(@ipset.set('test12').exists?).to be_falsey
    expect(@ipset.set('test13').exists?).to be_falsey
    expect(@ipset.set('test14').exists?).to be_falsey
  end

  it 'can destroy all sets' do
    # creating sets in another instance
    ipsetnext = Tablomat::IPSet.new
    ipsetnext.create(set_name: 'test14', type: 'hash:net')
    expect(@ipset.set('test14').exists?).to be_truthy
    ipsetnext.destroy_all(true)
    expect(@ipset.set('test10').exists?).to be_falsey
    expect(@ipset.set('test11').exists?).to be_falsey
    expect(@ipset.set('test14').exists?).to be_falsey
  end
end
