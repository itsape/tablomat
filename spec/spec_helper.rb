# frozen_string_literal: true

require 'simplecov'
require 'simplecov-cobertura'

SimpleCov.formatters = [
  SimpleCov::Formatter::CoberturaFormatter,
  SimpleCov::Formatter::HTMLFormatter
]

SimpleCov.start do
  enable_coverage :branch
  add_filter '/spec/'
end

require 'rubygems'
require 'bundler/setup'
require 'tablomat'

RSpec.configure do |config|
  # RSpec::Core::Configuration#treat_symbols_as_metadata_keys_with_true_values= is deprecated, it is now set to true as default and setting it to false has no effect.
  # config.treat_symbols_as_metadata_keys_with_true_values = true
  config.raise_errors_for_deprecations!
  config.run_all_when_everything_filtered = true
  config.filter_run :focus

  config.order = 'random'
end
