# frozen_string_literal: true

require 'spec_helper'
require 'etc'

describe Tablomat::IP6Tables, Tablomat::IPSet do
  describe 'Initialize module' do
    @ipset = Tablomat::IPSet.new
    @iptables = Tablomat::IP6Tables.new
  end

  before(:all) do
    @command = 'ipset'
    @command = "sudo #{@command}" if Etc.getlogin != 'root'
    @ipset = Tablomat::IPSet.new
    @iptables = Tablomat::IP6Tables.new
    @iptables.activate
  end

  after(:all) do
    # cleanup table and ipsets
    @iptables.table('mangle').chain('custom', &:apply_delete)
    @ipset.destroy_all
  end

  it 'can create and delete rules using ipsets if set exists' do
    #  initialize ipset and create set
    @ipset.create(set_name: 'testset1', type: 'hash:ip', create_options: 'family inet6')
    #  create rule
    @iptables.append('mangle', 'custom', set: @ipset.matchset(set_name: 'testset1', flags: 'src'), protocol: :tcp, sport: 123)
    expect(@iptables.exists('mangle', 'custom', set: @ipset.matchset(set_name: 'testset1', flags: 'src'), protocol: :tcp, sport: 123)).to be_truthy
    @iptables.delete('mangle', 'custom', set: @ipset.matchset(set_name: 'testset1', flags: 'src'), protocol: :tcp, sport: 123)
    expect(@iptables.exists('mangle', 'custom', set: @ipset.matchset(set_name: 'testset1', flags: 'src'), protocol: :tcp, sport: 123)).to be_falsey
  end

  it 'can create and delete rules using ipsets with Options' do
    @iptables.append('mangle', 'custom', match: @ipset.matchset(set_name: 'testset1', flags: 'src', option: '--update-subcounters', negate_option: true, negate: true), protocol: :tcp, sport: 123)
    expect(@iptables.exists('mangle', 'custom', match: @ipset.matchset(set_name: 'testset1', flags: 'src', option: '--update-subcounters', negate_option: true, negate: true), protocol: :tcp, sport: 123)).to be_truthy
    @iptables.delete('mangle', 'custom', match: @ipset.matchset(set_name: 'testset1', flags: 'src', option: '--update-subcounters', negate_option: true, negate: true), protocol: :tcp, sport: 123)
    expect(@iptables.exists('mangle', 'custom', match: @ipset.matchset(set_name: 'testset1', flags: 'src', option: '--update-subcounters', negate_option: true, negate: true), protocol: :tcp, sport: 123)).to be_falsey
  end

  it 'can not create rules using a set if the set does not exist' do
    expect(@ipset.set('testset2').exists?).to be_falsey
    expect { @iptables.append('mangle', 'custom', match: @ipset.matchset(set_name: 'testset', flags: 'src'), protocol: :tcp, sport: 123) }.to raise_error(RuntimeError)
  end

  it 'can not destroy a set if a rule is using it' do
    @ipset.create(set_name: 'testset3', type: 'hash:ip', create_options: 'family inet6')
    @iptables.append('mangle', 'custom', match: @ipset.matchset(set_name: 'testset3', flags: 'src'), protocol: :tcp, sport: 123)
    expect(@iptables.exists('mangle', 'custom', match: @ipset.matchset(set_name: 'testset3', flags: 'src'), protocol: :tcp, sport: 123)).to be_truthy
    expect { @ipset.destroy(set_name: 'testset3') }.to raise_error(Tablomat::IPSetError)
    @iptables.delete('mangle', 'custom', match: @ipset.matchset(set_name: 'testset3', flags: 'src'), protocol: :tcp, sport: 123)
    expect(@iptables.exists('mangle', 'custom', match: @ipset.matchset(set_name: 'testset3', flags: 'src'), protocol: :tcp, sport: 123)).to be_falsey
    @ipset.destroy(set_name: 'testset3')
  end

  it 'lists the set in active rules, if a set is given' do
    @ipset.create(set_name: 'testset4', type: 'hash:ip', create_options: 'family inet6')
    @iptables.append('mangle', 'custom', match: @ipset.matchset(set_name: 'testset4', flags: 'src'), protocol: :tcp, sport: 123)
    active_rules = @iptables.get_active_rules('mangle', 'custom')
    expect(active_rules[-1][:match]).to eq('testset4')
    @iptables.delete('mangle', 'custom', match: @ipset.matchset(set_name: 'testset4', flags: 'src'), protocol: :tcp, sport: 123)
    @ipset.destroy(set_name: 'testset4')
  end

  it 'does not list a set in active rules, if no set is given' do
    @ipset.create(set_name: 'testset5', type: 'hash:ip', create_options: 'family inet6')
    @iptables.append('mangle', 'custom', source: 'aaaa::ffff', protocol: :tcp, sport: 123)
    active_rules = @iptables.get_active_rules('mangle', 'custom')
    expect(active_rules[-1].key?(:match)).to be_falsey
    @iptables.delete('mangle', 'custom', source: 'aaaa::ffff', protocol: :tcp, sport: 123)
    @ipset.destroy(set_name: 'testset5')
  end
end
