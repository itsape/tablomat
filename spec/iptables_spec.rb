# frozen_string_literal: true

require 'spec_helper'
require 'etc'

describe Tablomat::IPTables do
  describe 'Initialize module' do
    @iptables = Tablomat::IPTables.new
  end

  before(:all) do
    @command = 'iptables'
    @command = "sudo #{@command}" if Etc.getlogin != 'root'

    @iptables = Tablomat::IPTables.new
    @iptables.activate
  end

  after(:all) do
    # cleanup tables
    @iptables.table('mangle').chain('test', &:apply_delete)
    @iptables.table('mangle').chain('custom', &:apply_delete)
    @iptables.table('filter').chain('rspec', &:apply_delete)
    @iptables.table('nat').chain('rspec', &:apply_delete)
  end

  it 'does raise an error when getting an invalid ruleset' do
    output = <<~IPTABLE
      *nat
      *mangle
      :PREROUTING ACCEPT [57:17363]
      :INPUT ACCEPT [57:17363]
      :FORWARD ACCEPT [0:0]
      :OUTPUT ACCEPT [60:16575]
      :POSTROUTING ACCEPT [60:16575]
      COMMIT
      *filter
    IPTABLE
    expect { @iptables.parse_output(output) }.to raise_error(StandardError)
  end

  it 'converts rules to iptables-save format via tmp chain' do
    expect(@iptables.normalize(protocol: :tcp, sport: 123, source: '1.2.3.4')).to eq('-s 1.2.3.4/32 -p tcp -m tcp --sport 123')
    expect(@iptables.normalize('-s 1.2.3.4/32 -p tcp -m tcp --sport 123')).to eq('-s 1.2.3.4/32 -p tcp -m tcp --sport 123')
    expect(@iptables.normalize('-s 1.2.3.4/32 -m tcp --sport 123 -p tcp')).to eq('-s 1.2.3.4/32 -p tcp -m tcp --sport 123')
  end

  it 'gets current ruleset' do
    # remove all existing rules in mangle FORWARD (hurts running host rules during test)
    `#{@command}-save > /tmp/iptables.save`
    `#{@command} -t mangle -F FORWARD`
    `#{@command} -t mangle -A FORWARD -p tcp -j ACCEPT`
    @iptables.synchronize

    `#{@command}-restore < /tmp/iptables.save`
    # @iptables.print

    # check that filter:INPUT is not owned!
    expect(@iptables.table('filter').chain('INPUT').owned).to be_falsey

    expect(@iptables.table('mangle').chain('FORWARD').rule(@iptables.normalize(protocol: :tcp, jump: :ACCEPT)).owned).to be_falsey
    expect(@iptables.table('mangle').chain('FORWARD').rule(@iptables.normalize(protocol: :tcp, jump: :ACCEPT)).active).to be_truthy
  end

  it 'creates new chain in table mangle' do
    @iptables.table('mangle').chain('custom').activate
    expect(@iptables.table('mangle').chain('custom').active).to be_truthy
  end

  it 'allows to retrieve active rules' do
    @iptables.append('nat', 'PREROUTING', source: '127.0.0.2',
                                          destination: '127.0.0.3',
                                          protocol: :tcp,
                                          dport: 8080,
                                          jump: 'DNAT', to: '127.0.0.4:9090')
    target_hash = { source: '127.0.0.2', destination: '127.0.0.3', dport: '8080', jump: 'DNAT', protocol: 'tcp', to_dest: '127.0.0.4:9090' }
    rules = @iptables.get_active_rules
    expect(rules.length).to eq(2)
    expect(rules.pop).to eq(target_hash)
    @iptables.delete('nat', 'PREROUTING', source: '127.0.0.2',
                                          destination: '127.0.0.3',
                                          protocol: :tcp,
                                          dport: 8080,
                                          jump: 'DNAT', to: '127.0.0.4:9090')
  end

  it 'is not possible to set policy for non builtin chains' do
    expect do
      @iptables.table('mangle').chain('custom') do |chain|
        chain.policy 'RETURN'
        chain.apply
      end
    end.to raise_error('Unable to assign policy to non builtin chains, TODO: implement handling')
  end

  #  it "can set policy for builtin chains" do
  #    @iptables.table("mangle").chain("INPUT") do | chain |
  #      chain.set_policy "ACCEPT"
  #    end
  #    @iptables.activate
  #  end

  it 'can remove chains' do
    # create chain
    @iptables.table('mangle').chain('test').activate
    expect(@iptables.exists('mangle', 'test')).to be_truthy
    @iptables.table('mangle').chain('test').deactivate
    expect(@iptables.exists('mangle', 'test')).to be_falsey
  end

  it 'checks if rule already exists' do
    # check precondition
    expect(@iptables.exists('mangle', 'custom', protocol: :tcp, sport: 123)).to be_falsy

    # check existing
    @iptables.append('mangle', 'custom', protocol: :tcp, sport: 123)
    expect(@iptables.exists('mangle', 'custom', protocol: :tcp, sport: 123)).to be_truthy
    expect(@iptables.table('mangle').chain('FORWARD').rule(@iptables.normalize(protocol: :tcp, jump: :ACCEPT)).active).to be_truthy

    # check missing
    @iptables.delete('mangle', 'custom', protocol: :tcp, sport: 123)
    expect(@iptables.exists('mangle', 'custom', protocol: :tcp, sport: 123)).to be_falsy
  end

  it 'appends new rule via system' do
    expect(@iptables.exists('mangle', 'custom', protocol: :tcp, sport: 123)).to be_falsy
    @iptables.append('mangle', 'custom', protocol: :tcp, sport: 123)
    expect(@iptables.table('mangle').chain('FORWARD').rule(@iptables.normalize(protocol: :tcp, jump: :ACCEPT)).owned).to be_falsey
    expect(@iptables.table('mangle').chain('FORWARD').rule(@iptables.normalize(protocol: :tcp, jump: :ACCEPT)).active).to be_truthy
  end

  it 'can remove rules via system' do
    expect(@iptables.exists('mangle', 'custom', protocol: :tcp, sport: 123)).to be_truthy
    @iptables.delete('mangle', 'custom', protocol: :tcp, sport: 123)
    expect(@iptables.exists('mangle', 'custom', protocol: :tcp, sport: 123)).to be_falsey
  end

  it 'changes all rules for a specific source address to another source address' do
    @iptables.append('filter', 'rspec', protocol: :tcp, sport: 123, source: '1.2.3.5')
    @iptables.switch_sources('1.2.3.5', '5.3.2.1')
    expect(@iptables.exists('filter', 'rspec', source: '5.3.2.1', protocol: :tcp, sport: 123)).to be_truthy
    expect(@iptables.exists('filter', 'rspec', source: '1.2.3.5', protocol: :tcp, sport: 123)).to be_falsey
    @iptables.append('nat', 'rspec', protocol: :tcp, sport: 123, source: '1.2.3.5', jump: :DNAT, 'to-destination': '127.0.0.1:123')
    @iptables.switch_sources('1.2.3.5', '5.3.2.1')
    expect(@iptables.exists('nat', 'rspec', source: '5.3.2.1', protocol: :tcp, sport: 123, jump: :DNAT, 'to-destination': '127.0.0.1:123')).to be_truthy
    expect(@iptables.exists('nat', 'rspec', source: '1.2.3.5', protocol: :tcp, sport: 123, jump: :DNAT, 'to-destination': '127.0.0.1:123')).to be_falsey
  end
end
