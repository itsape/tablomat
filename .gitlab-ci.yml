stages:
  - test
  - build
  - tag
  - release


.dockerd: &dockerd
  image: registry.gitlab.com/itsape/platform/image:latest
  tags: [itsape, docker]
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths: [vendor/]


.setup-bundler-env: &setup-bundler-env
    - export GEM_HOME="$CI_PROJECT_DIR/vendor"
    - export GEM_PATH="$GEM_PATH:$CI_PROJECT_DIR/vendor"
    - export BUNDLE_PATH="$CI_PROJECT_DIR/vendor"
    - export BUNDLE_APP_CONFIG="$CI_PROJECT_DIR/vendor"
    - export PATH="$PATH:$CI_PROJECT_DIR/vendor/bin"
    - mkdir -p "$CI_PROJECT_DIR/vendor"
    - gem install bundler
    - bundle config set path "$CI_PROJECT_DIR/vendor"
    - bundle config set without 'production'

# --------------------------------------------------------------------------------- test
rubocop:
  <<: *dockerd
  stage: test
  except: [schedules]
  script:
    - *setup-bundler-env
    - bundle install --jobs=$(nproc)  | grep -v "^Using"
    - bundle exec rubocop --format s -c .rubocop.yml


rspec:
  stage: test
  except: [schedules]
  tags:
    - itsape
    - vbox
  script:
    - cd ~/builds/itsape/tablomat/
    - sudo gem install bundler
    - bundler install
    - bundle exec rspec
    - pwd
    - ls coverage
  artifacts:
    paths: [coverage]
    expire_in: 1 week
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/coverage.xml

# --------------------------------------------------------------------------------- build
build:gem:
  <<: *dockerd
  stage: build
  except: [schedules]
  artifacts:
    paths: [tablomat*.gem, metadata/*]
    expire_in: 1 week
  script:
    - *setup-bundler-env
    - gem build tablomat.gemspec
    - mkdir metadata
    - VERSION=`find . -maxdepth 1  -printf "%f\n" | grep -Po '(?<=tablomat-)[\d\.]+(?=\.gem)'`
    - CLVERSION=$(head -n 1 changelog.md | grep -Po '\d+\.\d+\.\d+')
    - if [ "$VERSION" != "$CLVERSION" ]; then echo -e "\e[1m\e[91m\e[5mChangelog entry missing"; exit 1; fi
    - echo "$VERSION" > metadata/version
    - sed '/^$/q' changelog.md > metadata/description


# --------------------------------------------------------------------------------- tag
tag:
  stage: tag
  only: [master]
  except: [schedules, tags]
  dependencies: ["build:gem"]
  <<: *dockerd
  script:
    - VERSION="$(cat metadata/version)"
    - if [ $(git tag -l | grep "$VERSION") ]; then echo "\e[33m\e[1mVersion $VERSION already exists"; exit 0; fi
    - git config user.email "8388805-roboape@users.noreply.gitlab.com"
    - git config user.name "Roboape"
    - git remote add api-origin https://roboape:${ROBOAPE_TOKEN}@gitlab.com/${CI_PROJECT_PATH}
    - git tag -a $VERSION -m "Version $VERSION"
    - git push api-origin $VERSION


# --------------------------------------------------------------------------------- release
release:gitlab:
  stage: release
  only:
    refs:
      - tags
      - master
    variables:
      - $CI_COMMIT_TAG =~ /^?[0-9]+[.][0-9]+([.][0-9]+)?$/
  except: [schedules]
  dependencies: ["build:gem"]
  tags: [itsape, docker]
  image: registry.gitlab.com/gitlab-org/release-cli
  script:
    - VERSION="$(cat metadata/version)"
    - release-cli create --name "$VERSION" --description metadata/description --tag-name "$VERSION" --assets-link="{\"name\":\"tablomat-$VERSION.gem\", \"url\":\"https://rubygems.org/downloads/tablomat-$VERSION.gem\", \"type\":\"Package\"}"


release:rubygems:
  stage: release
  only:
    refs:
      - tags
      - master
    variables:
      - $CI_COMMIT_TAG =~ /^?[0-9]+[.][0-9]+([.][0-9]+)?$/
  except: [schedules]
  dependencies: ["build:gem"]
  <<: *dockerd
  script:
    - VERSION=`cat metadata/version`
    - gem push "tablomat-$VERSION.gem"
